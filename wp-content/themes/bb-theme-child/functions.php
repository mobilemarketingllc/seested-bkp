<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.min.js","","",1);
});

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js", array(), '3.2.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );


// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


function register_shortcodes(){
  $dir=dirname(__FILE__)."/shortcodes";
  $files=scandir($dir);
  foreach($files as $k=>$v){
    $file=$dir."/".$v;
    if(strstr($file,".php")){
      $shortcode=substr($v,0,-4);
      add_shortcode($shortcode,function($attr,$content,$tag){
        ob_start();
        include(dirname(__FILE__)."/shortcodes/".$tag.".php");
        $c=ob_get_clean();
        return $c;
      });
    }
  }
}



// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );


function featured_products_fun() {
	$args = array(
        'post_type' => array('carpeting', 'luxury_vinyl_tile', 'solid_wpc_waterproof', 'hardwood_catalog', 'laminate_catalog', 'tile_catalog'),
        'post__not_in' => array(323965,302354), 
        'posts_per_page' => 20,
        'orderby' => 'rand',
        'meta_query' => array(
            array(
                'key' => 'featured', 
                'value' => true, 
                'compare' => '=='
            )
        ) 
    );
    $query = new WP_Query( $args );
    if( $query->have_posts() ){
        $outpout = '<div class="featured-products"><div class="featured-product-list">';
        while ($query->have_posts()) : $query->the_post(); 
            // $gallery_images = get_field('gallery_room_images');
            // $gallery_img = explode("|",$gallery_images);
            // $count = 0;
            // // loop through the rows of data
            // foreach($gallery_img as  $key=>$value) {
            //     $room_image = $value;
            //     if(!$room_image){
            //         $room_image = "http://placehold.it/245x310?text=No+Image";
            //     }
            //     else{
            //         if(strpos($room_image , 's7.shawimg.com') !== false){
            //             if(strpos($room_image , 'http') === false){ 
            //                 $room_image = "http://" . $room_image;
            //             }
            //             $room_image = $room_image ;
            //         } else{
            //             if(strpos($room_image , 'http') === false){ 
            //                 $room_image = "https://" . $room_image;
            //             }
            //             $room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[524x636]&sink";
            //         }
            //     }
            //     if($count>0){
            //         break;
            //     }
            //     $count++;
            // }

            $image = get_field('swatch_image_link') ? get_field('swatch_image_link'):"http://placehold.it/168x123?text=No+Image"; 

            if(strpos($image , 's7.shawimg.com') !== false){
                if(strpos($image , 'http') === false){ 
                    $image = "http://" . $image;
                }	
                $image_thumb = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[150]&sink";
                $image = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[300x300]&sink";
            }
            else{
                if(strpos($image , 'http') === false){ 
                    $image = "https://" . $image;
                }	
                $image_thumb = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[150]&sink";
                $image = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[600x400]&sink";
            }

            $color_name = get_field('color');
            $post_type = get_post_type_object(get_post_type( get_the_ID() ));


            $brand = get_field('brand');
            $collection = get_field('collection');

            $productName = $brand.' '.$collection;

            $in_stock = get_field('in_stock', get_the_ID());     
            if($in_stock==1){
                $in_stock_txt = '<span class="stock-txt">Best Value</span>';
            }
            else{
                $in_stock_txt = '';
            }

            $familysku = get_post_meta(get_the_ID(), 'collection', true);
            $args1 = array(
                'post_type'      => array('carpeting', 'luxury_vinyl_tile', 'solid_wpc_waterproof', 'hardwood_catalog', 'laminate_catalog', 'tile_catalog'),
                'posts_per_page' => -1,
                'post_status'    => 'publish',
                'meta_query'     => array(
                    array(
                        'key'     => 'collection',
                        'value'   => $familysku,
                        'compare' => '='
                    )
                )
            );
            $the_query = new WP_Query( $args1 );

            
            $outpout .= '<div class="featured-product-item">
                <div class="featured-inner">
                    <div class="prod-img-wrapper">
                        '.$in_stock_txt.'
                        <a href="'.get_the_permalink().'" class="prod-img-wrap"><img src="'.$image.'" alt="'.get_the_title().'" /></a>
                    </div>
                    <div class="product-info">
                        <h2><a href="'.get_the_permalink().'">'.$color_name.'</a></h2>
                        <h5>'.$brand.'</h5>
                        <h4>'.$the_query->found_posts.' Colors</h4>
                    </div>
                </div>
            </div>';

        endwhile;
        $outpout .= '</div></div>';
        wp_reset_query();
    }  


    return $outpout;
}
add_shortcode( 'featured_products', 'featured_products_fun' );

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail',90 );

function wpse_override_yoast_breadcrumb_trail( $links ) {

    if (is_singular( 'carpeting' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/browse-carpet',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    if (is_singular( 'hardwood_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/hardwood/',
            'text' => 'Hardwood Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/browse-hardwood',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    if (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/laminate/',
            'text' => 'Laminate Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/browse-laminate',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    if (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/luxury-vinyl/',
            'text' => 'Luxury Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/browse-luxury-vinyl',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
    if (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/browse-ceramic',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
    
    return $links;
}


wp_clear_scheduled_hook( '404_redirection_301_log_cronjob' );

if (!wp_next_scheduled('flooring101_404_redirection_301_log_cronjob')) {    
   
    wp_schedule_event( time() +  17800, 'daily', 'flooring101_404_redirection_301_log_cronjob');
}

add_action( 'flooring101_404_redirection_301_log_cronjob', 'flooring101_custom_404_redirect_hook' ); 



function check_404_flooring101($url) {
   $headers=get_headers($url, 1);
   if ($headers[0]!='HTTP/1.1 200 OK') {return true; }else{ return false;}
}

 // custom 301 redirects from  404 logs table
 function flooring101_custom_404_redirect_hook(){
    global $wpdb;    
    write_log('in function');

    $table_redirect = $wpdb->prefix.'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix.'redirection_groups';

     $data_404 = $wpdb->get_results( "SELECT * FROM $table_name" );
     $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
     $redirect_group =  $datum[0]->id;  

    if ($data_404)
    {      
      foreach ($data_404 as $row_404) 
       {            

        if (strpos($row_404->url,'carpet') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {
            write_log($row_404->url);      
            
           $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404_flooring101($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

           
            $destination_url_carpet = '/browse-carpet/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_carpet,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            write_log( 'carpet 301 added ');
         }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');


         }

        }
        else if (strpos($row_404->url,'hardwood') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404_flooring101($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            $destination_url_hardwood = '/browse-hardwood/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_hardwood,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }

            write_log( 'hardwood 301 added ');

        }
        else if (strpos($row_404->url,'laminate') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404_flooring101($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            $destination_url_laminate = '/browse-laminate/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" =>$row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_laminate,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'laminate 301 added ');


        }
        else if ((strpos($row_404->url,'luxury-vinyl') !== false || strpos($row_404->url,'vinyl') !== false) && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);  

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404_flooring101($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            if (strpos($row_404->url,'luxury-vinyl') !== false ){
                $destination_url_lvt = '/browse-luxury-vinyl/';
            }elseif(strpos($row_404->url,'vinyl') !== false){
                $destination_url_lvt = '/browse-luxury-vinyl/';
            }
            
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" =>$row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_lvt,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
            }

            write_log( 'luxury-vinyl 301 added ');
        }
        else if (strpos($row_404->url,'tile') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404_flooring101($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $destination_url_tile = '/browse-ceramic/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_tile,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'tile 301 added ');
        }

       }  
    }

 }
 