<?php
/***
 * Implement the main component
 * and fire in on wp_footer
 *
 * @since 4.0.0
 */
function acwp_main_component() {
    $side = (get_option('acwp_toolbar_side') == 'right') ? 'acwp-right' : '';
    if( get_option('acwp_mobile') == 'yes')
        echo '<div class="acwp-toolbar '.$side.'" id="acwp-toolbar" tabindex="0" aria-label="'.__('Accessibility Toolbar Toggle View', 'acwp').'"></div>';
    elseif( !wp_is_mobile() )
        echo '<div class="acwp-toolbar '.$side.'" id="acwp-toolbar" tabindex="0" aria-label="'.__('Accessibility Toolbar Toggle View', 'acwp').'"></div>';
}
add_action('wp_footer', 'acwp_main_component');