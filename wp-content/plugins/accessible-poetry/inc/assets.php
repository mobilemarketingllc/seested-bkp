<?php
/**
 * Enqueueing our JS & CSS files for the front
 *
 * @since 4.0.0
 */
function acwp_front_assets() {
    $assets_dir_url = WPRS_DIR . 'assets';

    wp_enqueue_script(  'acwp-func',     $assets_dir_url . '/js/func.js',        array( 'jquery' ), '', true );
    wp_enqueue_script(  'acwp-public',   $assets_dir_url . '/js/public.min.js',  array( 'jquery' ), '', true );
    wp_localize_script( 'acwp-public',  'acwp_rest',      array(
            'acwp_settings' => rest_url( 'accessible-wp/v1/route/' ),
            'nonce' => wp_create_nonce( 'wp_rest' )
        )
    );

    wp_enqueue_style( 'acwp-main-css',      $assets_dir_url . '/css/main.css' );
    wp_enqueue_style( 'gmd-icons',          'https://fonts.googleapis.com/icon?family=Material+Icons' );
    wp_enqueue_style( 'acwp-colors-css',    $assets_dir_url . '/css/colors.css' );

    wp_enqueue_script("jquery-ui-draggable");
}
add_action( 'wp_enqueue_scripts', 'acwp_front_assets' );

/**
 * Enqueueing our JS & CSS files for the admin
 *
 * @since 4.0.0
 */
function acwp_admin_assets() {
    $assets_dir_url = WPRS_DIR . 'assets';

    wp_enqueue_script( 'acwp-admin',    $assets_dir_url . '/js/admin.js', array( 'jquery' ), '', true );
    wp_enqueue_style( 'acwp-admin-css', $assets_dir_url . '/css/admin.css' );
    wp_enqueue_media();
    wp_enqueue_style( 'wp-color-picker');
    wp_enqueue_script( 'wp-color-picker');
}
add_action('admin_enqueue_scripts', 'acwp_admin_assets');

/**
 * Update body classes according
 * to our settings
 *
 * @param $classes
 * @return array
 * @since 4.0.0
 */
function acwp_update_body_classes( $classes ) {
    $more = array();

    // Keyboard navigation - No border
    if( get_option('acwp_keyboard_noborder') == 'yes' )
        array_push($more, 'acwp-keyboard-noborder');

    // Keyboard navigation - No arrows
    if( get_option('acwp_keyboard_noarrows') == 'yes' )
        array_push($more, 'acwp-keyboard-noarrows');

    // Contrast - Custom colors
    if( get_option('acwp_contrast_custom') == 'yes' )
        array_push($more, 'acwp-contrast-custom');

    // Contrast - Hard CSS mode
    if( get_option('acwp_contrast_mode') == 'hard-css' )
        array_push($more, 'acwp-contrast-hardcss');

    if( get_option('acwp_contrast_mode') == 'js' )
        array_push($more, 'acwp-contrast-js');
    if( get_option('acwp_contrast_bgimages') == 'yes' )
        array_push($more, 'acwp-contrast-bgimages');
    if( get_option('acwp_titles_customcolors') == 'yes' )
        array_push($more, 'acwp-titles-custom');
    if( get_option('acwp_links_customcolors') == 'yes' )
        array_push($more, 'acwp-links-custom');
    if( get_option('acwp_titles_mode') == 'hard-css' )
        array_push($more, 'acwp-titles-hardcss');
    if( get_option('acwp_underline_mode') == 'hard-css' )
        array_push($more, 'acwp-underline-hardcss');
    if( get_option('acwp_links_mode') == 'hard-css' )
        array_push($more, 'acwp-links-hardcss');
    if( get_option('acwp_readable_mode') == 'hard-css' )
        array_push($more, 'acwp-readable-hardcss');

    return array_merge( $classes, $more );
}
add_filter( 'body_class', 'acwp_update_body_classes');

function acwp_contrast_customcolors_output() {
    $bg = get_option('acwp_contrast_bgs');
    $txt = get_option('acwp_contrast_txt');
    $lnk = get_option('acwp_contrast_links');
    ?>
    <style>
        body.acwp-contrast-custom.acwp-contrast * {
        <?php if( $bg != '' ) : ?>
            background-color: <?php echo $bg;?>;
        <?php endif; ?>
        <?php if( $txt != '' ) : ?>
            color: <?php echo $txt;?>;
        <?php endif; ?>
        }
        <?php if( $lnk != '' ) : ?>
        body.acwp-contrast-custom.acwp-contrast button,
        body.acwp-contrast-custom.acwp-contrast a, {
            color: <?php echo $lnk;?>;
        }
        <?php endif; ?>
    </style>
    <?php
}
if( get_option('acwp_contrast_custom') == 'yes' ) {
    add_filter( 'wp_head', 'acwp_contrast_customcolors_output');
}
function acwp_titles_custom_colors() {
    $bg = get_option('acwp_titles_bg');
    $txt = get_option('acwp_titles_txt');
    if( $bg != '' || $txt != '' ) :
        ?>
        <style>
            body.acwp-marktitles.acwp-titles-custom h1,
            body.acwp-marktitles.acwp-titles-custom h2,
            body.acwp-marktitles.acwp-titles-custom h3 {
            <?php if($bg != '') : ?>
                background-color: <?php echo $bg; ?>;
            <?php endif; ?>
            <?php if($txt != '') : ?>
                color: <?php echo $txt; ?>;
            <?php endif; ?>
            }
            body.acwp-marktitles.acwp-titles-custom.acwp-titles-hardcss h1,
            body.acwp-marktitles.acwp-titles-custom.acwp-titles-hardcss h2,
            body.acwp-marktitles.acwp-titles-custom.acwp-titles-hardcss h3 {
            <?php if($bg != '') : ?>
                background-color: <?php echo $bg; ?> !important;
            <?php endif; ?>
            <?php if($txt != '') : ?>
                color: <?php echo $txt; ?> !important;
            <?php endif; ?>
            }

        </style>
    <?php
    endif;
}
if( get_option('acwp_titles_customcolors') == 'yes' ) {
    add_filter( 'wp_head', 'acwp_titles_custom_colors');
}
function acwp_links_custom_colors() {
    $bg = get_option('acwp_links_bg');
    $txt = get_option('acwp_links_txt');
    if( $bg != '' || $txt != '' ) :
        ?>
        <style>
            body.acwp-marklinks.acwp-links-custom a,
            body.acwp-marklinks.acwp-links-custom button {
            <?php if($bg != '') : ?>
                background-color: <?php echo $bg; ?>;
            <?php endif; ?>
            <?php if($txt != '') : ?>
                color: <?php echo $txt; ?>;
            <?php endif; ?>
            }
            body.acwp-marklinks.acwp-links-custom.acwp-links-hardcss a,
            body.acwp-marklinks.acwp-links-custom.acwp-links-hardcss button {
            <?php if($bg != '') : ?>
                background-color: <?php echo $bg; ?> !important;
            <?php endif; ?>
            <?php if($txt != '') : ?>
                color: <?php echo $txt; ?> !important;
            <?php endif; ?>
            }

        </style>
    <?php
    endif;
}
if( get_option('acwp_links_customcolors') == 'yes' ) {
    add_filter( 'wp_head', 'acwp_links_custom_colors');
}

function acwp_readable_customfont_tahoma( $classes ) {
    return array_merge( $classes, array( 'acwp-readable-tahoma' ) );
}
function acwp_readable_customfont_arial( $classes ) {
    return array_merge( $classes, array( 'acwp-readable-arial' ) );
}
function acwp_readable_customfont_custom( $classes ) {
    return array_merge( $classes, array( 'acwp-readable-custom' ) );
}

function acwp_readable_custom_font() {
    $font = get_option('acwp_readable_custom');
    if( $font != '' ) :
        ?>
        <style>
            body.acwp-readable:not(.acwp-readable-hardcss), body.acwp-readable:not(.acwp-readable-hardcss) * {
                font-family: <?php echo $font;?>;
            }
            body.acwp-readable.acwp-readable-hardcss, body.acwp-readable.acwp-readable-hardcss * {
                font-family: <?php echo $font;?> !important;
            }
        </style>
    <?php
    endif;
}

if( get_option('acwp_readable_font') == 'tahome' )
    add_filter( 'body_class', 'acwp_readable_customfont_tahoma');
elseif( get_option('acwp_readable_font') != 'custom' && get_option('acwp_readable_font') != 'tahoma' )
    add_filter( 'body_class', 'acwp_readable_customfont_arial');
elseif( get_option('acwp_readable_font') == 'custom' ) {
    add_filter( 'body_class', 'acwp_readable_customfont_custom');
    add_action('wp_head', 'acwp_readable_custom_font');
}



