<?php
/**
 * Route Settings
 *
 * Declare our settings over the acwp route
 *
 * @param $data
 * @return array
 * @since 4.0.0
 */
function acwp_route_settings( $data ) {
    $mode = (is_rtl()) ? 'rtl' :'ltr';
    return array(
        // Global
        'direction_mode'  => get_option('acwp_toolbar_side'),
        'text_heading'  => __('Text', 'acwp'),
        'show_on_mobile' => get_option('acwp_mobile'),
        'toolbar_customcolors' => get_option('acwp_toolbar_customcolors'),
        'linkscolor' => get_option('acwp_linkscolor'),

        // Bottom
        'statement' => get_option('acwp_statement'),
        'statement_label' => get_option('acwp_statement_label'),

        'feedback' => get_option('acwp_feedback'),
        'feedback_label' => get_option('acwp_feedback_label'),

        // Button
        'toggle_fromtop' => get_option('acwp_toggle_fromtop'),
        'toggle_fromside' => get_option('acwp_toggle_fromside'),

        // Heading
        'heading_title' => (get_option('acwp_heading_title') != '') ? get_option('acwp_heading_title') : '',
        'heading_desc' => (get_option('acwp_heading_desc') != '') ? get_option('acwp_heading_desc') : '',
        'heading_bg' => get_option('acwp_heading_bg'),
        'heading_textcolor' => get_option('acwp_heading_textcolor'),

        // Keyboard navigation
        'keyboard_label' => (get_option('acwp_label_keyboard') != '') ? get_option('acwp_label_keyboard') : 'Keyboard Navigation',
        'keyboard_hide' => get_option('acwp_hide_keyboard'),
        'keyboard_noborder' => get_option('acwp_keyboard_noborder'),
        'keyboard_noarrows' => get_option('acwp_keyboard_noarrows'),

        // Contrast
        'contrast_heading'  => __('Contrast', 'acwp'),
        'contrast_label'    => (get_option('acwp_label_contrast') != '') ? get_option('acwp_label_contrast') : 'Dark Contrast',
        'contrast_hide'     => get_option('acwp_hide_contrast'),
        'contrast_bgimages' => get_option('acwp_contrast_bgimages'),

        // Monochrome
        'monochrome_label' => (get_option('acwp_label_monochrome') != '') ? get_option('acwp_label_monochrome') : 'Disable Colors',
        'monochrome_hide' => get_option('acwp_hide_monochrome'),

        // Disable animations
        'animations_label' => (get_option('acwp_label_animations') != '') ? get_option('acwp_label_animations') : 'Disable Animations',
        'animations_hide' => get_option('acwp_hide_animations'),

        // Change font size
        'incfont_label' => (get_option('acwp_label_incfont') != '') ? get_option('acwp_label_incfont') : 'Increase Text Size',
        'decfont_label' => (get_option('acwp_label_decfont') != '') ? get_option('acwp_label_decfont') : 'Decrease Text Size',
        'fontsize_hide' => get_option('acwp_hide_fontsize'),
        'incfont_size' => get_option('acwp_incfont_size'),
        'decfont_size' => get_option('acwp_decfont_size'),
        'fontsize_customtags' => get_option('acwp_fontsize_customtags'),
        'fontsize_tags' => get_option('acwp_fontsize_tags'),
        'fontsize_nolineheight' => get_option('acwp_fontsize_nolineheight'),

        // Readable font
        'readable_label' => (get_option('acwp_label_readable') != '') ? get_option('acwp_label_readable') : 'Readable Font',
        'readable_hide' => get_option('acwp_hide_readable'),

        // Mark titles
        'titles_label' => (get_option('acwp_label_titles') != '') ? get_option('acwp_label_titles') : 'Mark Titles',
        'titles_hide' => get_option('acwp_hide_titles'),

        // Mark links
        'links_label' => (get_option('acwp_label_links') != '') ? get_option('acwp_label_links') : 'Mark Links',
        'links_hide' => get_option('acwp_hide_links'),

        // Underline links
        'underline_label' => (get_option('acwp_label_underline') != '') ? get_option('acwp_label_underline') : 'Underline Links',
        'underline_hide' => get_option('acwp_hide_underline'),
    );
}

/**
 * Declare our route and it's callback
 *
 * @since 4.0.0
 */
add_action( 'rest_api_init', function () {
    register_rest_route( 'accessible-wp/v1', '/route', array(
        'methods' => 'GET',
        'callback' => 'acwp_route_settings',
    ) );
} );

