<?php

/**
 * Class ACWP_AdminPanel
 *
 * Register our admin pages and our settings for the toolbar
 *
 * @since 4.0.0
 */
class ACWP_AdminPanel {

    public function __construct() {

        // Register admin pages
        add_action( 'admin_menu', array(&$this, 'register_pages') );

        // Register settings
        add_action( 'admin_init', array(&$this, 'register_settings') );
        add_action( 'rest_api_init', array(&$this, 'register_settings') );
    }

    public function register_pages() {

        // Check if we already got the primary page of AccessibleWP if not we will add it
        if ( empty ($GLOBALS['admin_page_hooks']['accessible-wp'] ) ) {
            add_menu_page('AccessibleWP', 'AccessibleWP', 'read', 'accessible-wp', array($this, 'main_page_callback'), plugins_url( 'accessible-wp-toolbar/assets/svg/accessible.svg' ), '2.1');
        }

        // Add our sub page for the Toolbar
        add_submenu_page('accessible-wp', 'AccessibleWP Toolbar', 'Toolbar', 'manage_options', 'accessiblewp-toolbar', array(&$this, 'submenu_page_callback'));
    }

    public function register_settings(){

        // Heading
        register_setting('acwp', 'acwp_heading_title',      array('show_in_rest' => true));
        register_setting('acwp', 'acwp_heading_desc',       array('show_in_rest' => true));

        // Toolbar Colors
        register_setting('acwp', 'acwp_heading_bg',             array('show_in_rest' => true));
        register_setting('acwp', 'acwp_heading_textcolor',      array('show_in_rest' => true));
        register_setting('acwp', 'acwp_toolbar_customcolors',   array('show_in_rest' => true));

        register_setting('acwp', 'acwp_toolbar_side',       array('show_in_rest' => true));
        register_setting('acwp', 'acwp_toggle_fromtop',     array('show_in_rest' => true));
        register_setting('acwp', 'acwp_toggle_fromside',    array('show_in_rest' => true));

        // Keyboard Navigation
        register_setting('acwp', 'acwp_keyboard_noborder',  array('show_in_rest' => true));
        register_setting('acwp', 'acwp_keyboard_noarrows',  array('show_in_rest' => true));
        register_setting('acwp', 'acwp_label_keyboard',     array('show_in_rest' => true));
        register_setting('acwp', 'acwp_hide_keyboard',      array('show_in_rest' => true));

        // Contrast
        register_setting('acwp', 'acwp_contrast_custom',    array('show_in_rest' => true));
        register_setting('acwp', 'acwp_contrast_bgs',       array('show_in_rest' => true));
        register_setting('acwp', 'acwp_contrast_txt',       array('show_in_rest' => true));
        register_setting('acwp', 'acwp_contrast_links',     array('show_in_rest' => true));
        register_setting('acwp', 'acwp_label_contrast',     array('show_in_rest' => true));
        register_setting('acwp', 'acwp_hide_contrast',      array('show_in_rest' => true));
        register_setting('acwp', 'acwp_contrast_mode',      array('show_in_rest' => true));

        // Font Size
        register_setting('acwp', 'acwp_fontsize_nolineheight',  array('show_in_rest' => true));
        register_setting('acwp', 'acwp_incfont_size',           array('show_in_rest' => true));
        register_setting('acwp', 'acwp_decfont_size',           array('show_in_rest' => true));
        register_setting('acwp', 'acwp_label_incfont',          array('show_in_rest' => true));
        register_setting('acwp', 'acwp_label_decfont',          array('show_in_rest' => true));
        register_setting('acwp', 'acwp_hide_fontsize',          array('show_in_rest' => true));
        register_setting('acwp', 'acwp_fontsize_customtags',    array('show_in_rest' => true));
        register_setting('acwp', 'acwp_fontsize_tags',          array('show_in_rest' => true));

        // Monochrome
        register_setting('acwp', 'acwp_label_monochrome',   array('show_in_rest' => true));
        register_setting('acwp', 'acwp_hide_monochrome',    array('show_in_rest' => true));

        // Disable Animations
        register_setting('acwp', 'acwp_animations_hard',    array('show_in_rest' => true));
        register_setting('acwp', 'acwp_label_animations',   array('show_in_rest' => true));
        register_setting('acwp', 'acwp_hide_animations',    array('show_in_rest' => true));

        // Readable font
        register_setting('acwp', 'acwp_readable_font',      array('show_in_rest' => true));
        register_setting('acwp', 'acwp_readable_custom',    array('show_in_rest' => true));
        register_setting('acwp', 'acwp_label_readable',     array('show_in_rest' => true));
        register_setting('acwp', 'acwp_hide_readable',      array('show_in_rest' => true));
        register_setting('acwp', 'acwp_readable_mode',      array('show_in_rest' => true));

        // Mark Titles
        register_setting('acwp', 'acwp_titles_bg',              array('show_in_rest' => true));
        register_setting('acwp', 'acwp_titles_txt',             array('show_in_rest' => true));
        register_setting('acwp', 'acwp_label_titles',           array('show_in_rest' => true));
        register_setting('acwp', 'acwp_hide_titles',            array('show_in_rest' => true));
        register_setting('acwp', 'acwp_titles_customcolors',    array('show_in_rest' => true));
        register_setting('acwp', 'acwp_titles_mode',            array('show_in_rest' => true));

        // Mark Links
        register_setting('acwp', 'acwp_links_bg',           array('show_in_rest' => true));
        register_setting('acwp', 'acwp_links_txt',          array('show_in_rest' => true));
        register_setting('acwp', 'acwp_label_links',        array('show_in_rest' => true));
        register_setting('acwp', 'acwp_hide_links',         array('show_in_rest' => true));
        register_setting('acwp', 'acwp_linkscolor',         array('show_in_rest' => true));
        register_setting('acwp', 'acwp_links_customcolors', array('show_in_rest' => true));
        register_setting('acwp', 'acwp_links_mode',         array('show_in_rest' => true));

        // Underline links
        register_setting('acwp', 'acwp_label_underline',    array('show_in_rest' => true));
        register_setting('acwp', 'acwp_underline_labels',   array('show_in_rest' => true));
        register_setting('acwp', 'acwp_underline_buttons',  array('show_in_rest' => true));
        register_setting('acwp', 'acwp_hide_underline',     array('show_in_rest' => true));

        // Mobile Visibility
        register_setting('acwp', 'acwp_mobile',     array('show_in_rest' => true));

        // Statement & Feedback
        register_setting('acwp', 'acwp_statement',  array('show_in_rest' => true));
        register_setting('acwp', 'acwp_feedback',   array('show_in_rest' => true));
        register_setting('acwp', 'acwp_statement_label',  array('show_in_rest' => true));
        register_setting('acwp', 'acwp_feedback_label',   array('show_in_rest' => true));
    }

    public function main_page_callback() {
        ?>
        <div class="wrap">
            <h1><?php _e('Accessibility', 'acwp');?></h1>
            <div id="welcome-panel" class="welcome-panel">
                <div class="welcome-panel-content">
                    <h2><?php _e('Welcome to AccessibleWP Dashboard!', 'acwp');?></h2>
                    <p class="about-description" style="max-width: 800px"><?php _e('This plugin was developed as part of our Accessibility plugins package to allow as much accessibility as possible to users across the network. we offer a variety of Accessibility plugins as you will find below.', 'acwp');?></p>
                    <div class="welcome-panel-column-container">
                        <div class="welcome-panel-column">
                            <h3><?php _e('Contact us', 'acwp');?></h3>
                            <a class="button button-primary button-hero load-customize hide-if-no-customize" href="https://www.codenroll.co.il/contact/" target="_blank"><?php _e('Our website', 'acwp');?></a>
                        </div>
                        <div class="welcome-panel-column">
                            <h3><?php _e('Our accessibility plugins', 'acwp');?></h3>
                            <ul>
                                <li><a href="https://www.wordpress.org/plugins/accessible-poetry/"><?php _e('Toolbar', 'acwp');?></a></li>
                                <li><a href="https://www.wordpress.org/plugins/accessiblewp-images/"><?php _e('Images', 'acwp');?></a></li>
                            </ul>
                        </div>
                        <div class="welcome-panel-column"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    public function submenu_tab_heading() {
        ?>
        <div id="acwp_heading" class="acwp-tab active">
            <h2><?php _e('Toolbar Heading', 'acwp');?></h2>

            <table class="form-table">
                <tr valign="top">
                    <th scope="row"><?php _e('Heading Title', 'acwp');?></th>
                    <td><input type="text" name="acwp_heading_title" value="<?php echo esc_attr( get_option('acwp_heading_title') ); ?>" placeholder="<?php _e('Accessibility Toolbar', 'acwp');?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Heading Description', 'acwp');?></th>
                    <td>
                        <textarea name="acwp_heading_desc" placeholder="<?php _e('Website Accessibility Adjustment Tools.', 'acwp'); ?>"><?php echo esc_attr( get_option('acwp_heading_desc') ); ?></textarea>
                    </td>
                </tr>
            </table>
        </div>
        <?php
    }
    public function submenu_tab_options() {
        ?>
        <div id="acwp_options" class="acwp-tab">
            <h2><?php _e('Options', 'acwp');?></h2>

            <table class="form-table">
                <tr class="acwp-tr-heading">
                    <th>
                        <h4><?php _e('Keyboard Navigation', 'acwp');?></h4>
                    </th>
                    <td>
                        <ul class="acwp-list">
                            <li><?php _e('Add red border to components in focus mode.', 'acwp');?></li>
                            <li><?php _e('Allow navigation between components using keyboard arrows.', 'acwp');?></li>
                        </ul>
                    </td>
                </tr>
                <tr valign="top" class="acwp-tr-first">
                    <th scope="row"><?php _e('Change Label', 'acwp');?></th>
                    <td><input type="text" name="acwp_label_keyboard" value="<?php echo esc_attr( get_option('acwp_label_keyboard') ); ?>" placeholder="<?php _e('Keyboard Navigation', 'acwp');?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e("Disable red border to components in focus mode", 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_keyboard_noborder" value="yes" <?php checked( esc_attr( get_option('acwp_keyboard_noborder') ), 'yes' ); ?> /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e("Disable arrows navigation between components", 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_keyboard_noarrows" value="yes" <?php checked( esc_attr( get_option('acwp_keyboard_noarrows') ), 'yes' ); ?> /></td>
                </tr>
                <tr valign="top" class="acwp-tr-last">
                    <th scope="row"><?php _e('Hide Option', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_hide_keyboard" value="yes" <?php checked( esc_attr( get_option('acwp_hide_keyboard') ), 'yes' ); ?> /></td>
                </tr>

                <tr class="acwp-tr-heading">
                    <th>
                        <h4><?php _e('Disable Animations', 'acwp');?></h4>
                    </th>
                    <td>
                        <ul class="acwp-list">
                            <li><?php _e('Disable CSS3 Animations with CSS class.', 'acwp');?></li>
                        </ul>
                    </td>
                </tr>
                <tr valign="top" class="acwp-tr-first">
                    <th scope="row"><?php _e('Change Label', 'acwp');?></th>
                    <td><input type="text" name="acwp_label_animations" value="<?php echo esc_attr( get_option('acwp_label_animations') ); ?>" placeholder="<?php _e('Disable Animations', 'acwp');?>" /></td>
                </tr>
                <tr valign="top" class="acwp-tr-last">
                    <th scope="row"><?php _e('Hide Option', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_hide_animations" value="yes" <?php checked( esc_attr( get_option('acwp_hide_animations') ), 'yes' ); ?> /></td>
                </tr>

                <tr class="acwp-tr-heading">
                    <th>
                        <h4><?php _e('Dark Contrast', 'acwp');?></h4>
                    </th>
                    <td>
                        <ul class="acwp-list">
                            <li><?php _e('Change the colors of backgrounds, texts & links.', 'acwp');?></li>
                            <li><?php _e('Disable background images.', 'acwp');?></li>
                        </ul>
                    </td>
                </tr>
                <tr valign="top" class="acwp-tr-first">
                    <th scope="row"><?php _e('Change Label', 'acwp');?></th>
                    <td><input type="text" name="acwp_label_contrast" value="<?php echo esc_attr( get_option('acwp_label_contrast') ); ?>" placeholder="<?php _e('Dark Contrast', 'acwp');?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Use custom colors', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_contrast_custom" id="acwp_contrast_custom" value="yes" <?php checked( esc_attr( get_option('acwp_contrast_custom') ), 'yes' ); ?> /></td>
                </tr>
                <tr valign="top" id="acwp-contrast-bgcolor-row" class="acwp-hide-row">
                    <th scope="row"><?php _e('Backgrounds Color', 'acwp');?></th>
                    <td><input type="color" name="acwp_contrast_bgs" class="color-field" value="<?php echo esc_attr( get_option('acwp_contrast_bgs') ); ?>" data-default-color="#000000" /></td>
                </tr>
                <tr valign="top" id="acwp-contrast-txtcolor-row" class="acwp-hide-row">
                    <th scope="row"><?php _e('Text Color', 'acwp');?></th>
                    <td><input type="color" name="acwp_contrast_txt" class="color-field" value="<?php echo esc_attr( get_option('acwp_contrast_txt') ); ?>" data-default-color="#ffffff" /></td>
                </tr>
                <tr valign="top" id="acwp-contrast-linkscolor-row" class="acwp-hide-row">
                    <th scope="row"><?php _e('Links Color', 'acwp');?></th>
                    <td><input type="color" name="acwp_contrast_links" class="color-field" value="<?php echo esc_attr( get_option('acwp_contrast_links') ); ?>" data-default-color="#ffff00" /></td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Mode', 'acwp');?></th>
                    <td>
                        <select name="acwp_contrast_mode" id="acwp_contrast_mode">
                            <option value="">-- <?php _e('Normal CSS (default)', 'acwp');?> --</option>
                            <option value="hard-css" <?php selected('hard-css', get_option('acwp_contrast_mode'))?>><?php _e('Hard CSS', 'acwp');?></option>
                            <option value="js" <?php selected('js', get_option('acwp_contrast_mode'))?>><?php _e('JS', 'acwp');?></option>
                        </select>
                    </td>
                </tr>
                <tr valign="top" class="acwp-tr-last">
                    <th scope="row"><?php _e('Hide Option', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_hide_contrast" value="yes" <?php checked( esc_attr( get_option('acwp_hide_contrast') ), 'yes' ); ?> /></td>
                </tr>

                <tr class="acwp-tr-heading">
                    <th>
                        <h4><?php _e('Monochrome', 'acwp');?></h4>
                    </th>
                    <td>
                        <ul class="acwp-list">
                            <li><?php _e('Change the colors of everything to grayscale.', 'acwp');?></li>
                        </ul>
                    </td>
                </tr>
                <tr valign="top" class="acwp-tr-first">
                    <th scope="row"><?php _e('Change Label', 'acwp');?></th>
                    <td><input type="text" name="acwp_label_monochrome" value="<?php echo esc_attr( get_option('acwp_label_monochrome') ); ?>" placeholder="<?php _e('Black & White Contrast', 'acwp');?>" /></td>
                </tr>
                <tr valign="top" class="acwp-tr-last">
                    <th scope="row"><?php _e('Hide Option', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_hide_monochrome" value="yes" <?php checked( esc_attr( get_option('acwp_hide_monochrome') ), 'yes' ); ?> /></td>
                </tr>

                <tr class="acwp-tr-heading">
                    <th>
                        <h4><?php _e('Change Text Size', 'acwp');?></h4>
                    </th>
                    <td>
                        <ul class="acwp-list">
                            <li><?php _e('Increase and decrease font sizes.', 'acwp');?></li>
                        </ul>
                    </td>
                </tr>
                <tr valign="top" class="acwp-tr-first">
                    <th scope="row"><?php _e('Change Label of Increase Text Size', 'acwp');?></th>
                    <td><input type="text" name="acwp_label_incfont" value="<?php echo esc_attr( get_option('acwp_label_incfont') ); ?>" placeholder="<?php _e('Increase Text Size', 'acwp');?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Change Label of Decrease Text Size', 'acwp');?></th>
                    <td><input type="text" name="acwp_label_decfont" value="<?php echo esc_attr( get_option('acwp_label_decfont') ); ?>" placeholder="<?php _e('Decrease Text Size', 'acwp');?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('By how many percent to increase the font size?', 'acwp');?></th>
                    <td><input type="number" name="acwp_incfont_size" value="<?php echo esc_attr( get_option('acwp_incfont_size') ); ?>" placeholder="160" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('By how many percent to decrease the font size?', 'acwp');?></th>
                    <td><input type="number" name="acwp_decfont_size" value="<?php echo esc_attr( get_option('acwp_decfont_size') ); ?>" placeholder="80" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Use custom tags?', 'acwp');?></th>
                    <td><input type="checkbox" id="acwp_fontsize_customtags" name="acwp_fontsize_customtags" value="yes" <?php checked( esc_attr( get_option('acwp_fontsize_customtags') ), 'yes' ); ?> /></td>
                </tr>
                <tr valign="top" class="acwp-hide-row" id="acwp-fontsize-tags-row">
                    <th scope="row"><?php _e('By how many percent to decrease the font size?', 'acwp');?> <small style="display: block;">(<?php _e('separate tags with comma', 'acwp');?></small></th>
                    <td><input type="text" name="acwp_fontsize_tags" value="<?php echo esc_attr( get_option('acwp_fontsize_tags') ); ?>" placeholder="p,h1,h2,h3,h4,h5,h6,label" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Disable line-height reset when size is changing', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_fontsize_nolineheight" value="yes" <?php checked( esc_attr( get_option('acwp_fontsize_nolineheight') ), 'yes' ); ?> /></td>
                </tr>

                <tr valign="top" class="acwp-tr-last">
                    <th scope="row"><?php _e('Hide Option', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_hide_fontsize" value="yes" <?php checked( esc_attr( get_option('acwp_hide_fontsize') ), 'yes' ); ?> /></td>
                </tr>

                <tr class="acwp-tr-heading">
                    <th>
                        <h4><?php _e('Readable Font', 'acwp');?></h4>
                    </th>
                    <td>
                        <ul class="acwp-list">
                            <li><?php _e('Change the font family of all text to readable font.', 'acwp');?></li>
                        </ul>
                    </td>
                </tr>
                <tr valign="top" class="acwp-tr-first">
                    <th scope="row"><?php _e('Change Label', 'acwp');?></th>
                    <td><input type="text" name="acwp_label_readable" value="<?php echo esc_attr( get_option('acwp_label_readable') ); ?>" placeholder="<?php _e('Readable Font', 'acwp');?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Mode', 'acwp');?></th>
                    <td>
                        <select name="acwp_readable_mode" id="acwp_readable_mode">
                            <option value="">-- <?php _e('Normal CSS (default)', 'acwp');?> --</option>
                            <option value="hard-css" <?php selected('hard-css', get_option('acwp_readable_mode'))?>><?php _e('Hard CSS', 'acwp');?></option>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Choose your readable font:', 'acwp');?></th>
                    <td>
                        <select name="acwp_readable_font" id="acwp_readable_font">
                            <option value="">-- <?php _e('Default (Arial)', 'acwp');?> --</option>
                            <option value="Tahoma" <?php selected('Tahoma', get_option('acwp_readable_font'))?>><?php _e('Tahoma', 'acwp');?></option>
                            <option value="custom" <?php selected('custom', get_option('acwp_readable_font'))?>><?php _e('Custom Family', 'acwp');?></option>
                        </select>
                    </td>
                </tr>
                <tr valign="top" id="acwp-row-readable-custom" class="acwp-hide-row">
                    <th scope="row"><?php _e('Custom Font Family', 'acwp');?></th>
                    <td><input type="text" name="acwp_readable_custom" value="<?php echo esc_attr( get_option('acwp_readable_custom') ); ?>" /></td>
                </tr>
                <tr valign="top" class="acwp-tr-last">
                    <th scope="row"><?php _e('Hide Option', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_hide_readable" value="yes" <?php checked( esc_attr( get_option('acwp_hide_readable') ), 'yes' ); ?> /></td>
                </tr>

                <tr class="acwp-tr-heading">
                    <th>
                        <h4><?php _e('Mark Titles', 'acwp');?></h4>
                    </th>
                    <td>
                        <ul class="acwp-list">
                            <li><?php _e('Add custom background color and text color to heading components.', 'acwp');?></li>
                        </ul>
                    </td>
                </tr>
                <tr valign="top" class="acwp-tr-first">
                    <th scope="row"><?php _e('Change Label', 'acwp');?></th>
                    <td><input type="text" name="acwp_label_titles" value="<?php echo esc_attr( get_option('acwp_label_titles') ); ?>" placeholder="<?php _e('Mark Titles', 'acwp');?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Mode', 'acwp');?></th>
                    <td>
                        <select name="acwp_titles_mode" id="acwp_titles_mode">
                            <option value="">-- <?php _e('Normal CSS (default)', 'acwp');?> --</option>
                            <option value="hard-css" <?php selected('hard-css', get_option('acwp_titles_mode'))?>><?php _e('Hard CSS', 'acwp');?></option>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Use custom colors?', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_titles_customcolors" id="acwp_titles_customcolors" value="yes" <?php checked( esc_attr( get_option('acwp_titles_customcolors') ), 'yes' ); ?> /></td>
                </tr>
                <tr valign="top" class="acwp-hide-row" id="acwp-titles-bg-row">
                    <th scope="row"><?php _e('Titles Background Color', 'acwp');?></th>
                    <td><input type="color" name="acwp_titles_bg" class="color-field" value="<?php echo esc_attr( get_option('acwp_titles_bg') ); ?>" data-default-color="#ffff00" /></td>
                </tr>
                <tr valign="top" class="acwp-hide-row" id="acwp-titles-txt-row">
                    <th scope="row"><?php _e('Titles Text Color', 'acwp');?></th>
                    <td><input type="color" name="acwp_titles_txt" class="color-field" value="<?php echo esc_attr( get_option('acwp_titles_txt') ); ?>" data-default-color="#000000" /></td>
                </tr>
                <tr valign="top" class="acwp-tr-last">
                    <th scope="row"><?php _e('Hide Option', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_hide_titles" value="yes" <?php checked( esc_attr( get_option('acwp_hide_titles') ), 'yes' ); ?> /></td>
                </tr>

                <tr class="acwp-tr-heading">
                    <th>
                        <h4><?php _e('Mark Links', 'acwp');?></h4>
                    </th>
                    <td>
                        <ul class="acwp-list">
                            <li><?php _e('Add custom background color and text color to heading components.', 'acwp');?></li>
                        </ul>
                    </td>
                </tr>
                <tr valign="top" class="acwp-tr-first">
                    <th scope="row"><?php _e('Change Label', 'acwp');?></th>
                    <td><input type="text" name="acwp_label_links" value="<?php echo esc_attr( get_option('acwp_label_links') ); ?>" placeholder="<?php _e('Mark Links', 'acwp');?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Mode', 'acwp');?></th>
                    <td>
                        <select name="acwp_links_mode" id="acwp_titles_mode">
                            <option value="">-- <?php _e('Normal CSS (default)', 'acwp');?> --</option>
                            <option value="hard-css" <?php selected('hard-css', get_option('acwp_links_mode'))?>><?php _e('Hard CSS', 'acwp');?></option>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Use custom colors?', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_links_customcolors" id="acwp_links_customcolors" value="yes" <?php checked( esc_attr( get_option('acwp_links_customcolors') ), 'yes' ); ?> /></td>
                </tr>
                <tr valign="top" class="acwp-hide-row" id="acwp-links-bgcolor-row">
                    <th scope="row"><?php _e('Links Background Color', 'acwp');?></th>
                    <td><input type="color" name="acwp_links_bg" class="color-field" value="<?php echo esc_attr( get_option('acwp_links_bg') ); ?>" data-default-color="#ffff00" /></td>
                </tr>
                <tr valign="top" class="acwp-hide-row" id="acwp-links-txtcolor-row">
                    <th scope="row"><?php _e('Links Text Color', 'acwp');?></th>
                    <td><input type="color" name="acwp_links_txt" class="color-field" value="<?php echo esc_attr( get_option('acwp_links_txt') ); ?>" data-default-color="#000000" /></td>
                </tr>
                <tr valign="top" class="acwp-tr-last">
                    <th scope="row"><?php _e('Hide Option', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_hide_links" value="yes" <?php checked( esc_attr( get_option('acwp_hide_links') ), 'yes' ); ?> /></td>
                </tr>

                <tr class="acwp-tr-heading">
                    <th>
                        <h4><?php _e('Underline Links', 'acwp');?></h4>
                    </th>
                    <td>
                        <ul class="acwp-list">
                            <li><?php _e('Add line under all link components.', 'acwp');?></li>
                        </ul>
                    </td>
                </tr>
                <tr valign="top" class="acwp-tr-first">
                    <th scope="row"><?php _e('Change Label', 'acwp');?></th>
                    <td><input type="text" name="acwp_label_underline" value="<?php echo esc_attr( get_option('acwp_label_underline') ); ?>" placeholder="<?php _e('Underline Links', 'acwp');?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Mode', 'acwp');?></th>
                    <td>
                        <select name="acwp_underline_mode" id="acwp_underline_mode">
                            <option value="">-- <?php _e('Normal CSS (default)', 'acwp');?> --</option>
                            <option value="hard-css" <?php selected('hard-css', get_option('acwp_underline_mode'))?>><?php _e('Hard CSS', 'acwp');?></option>
                        </select>
                    </td>
                </tr>
                <tr valign="top" class="acwp-tr-last">
                    <th scope="row"><?php _e('Hide Option', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_hide_underline" value="yes" <?php checked( esc_attr( get_option('acwp_hide_underline') ), 'yes' ); ?> /></td>
                </tr>
            </table>
        </div>
        <?php
    }
    public function submenu_tab_settings(){
        ?>
        <div id="acwp_settings" class="acwp-tab">
            <h2><?php _e('Settings', 'acwp');?></h2>

            <table class="form-table">
                <tr valign="top">
                    <th scope="row"><?php _e('Show toolbar also on mobile devices', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_mobile" value="yes" <?php checked( esc_attr( get_option('acwp_mobile') ), 'yes' ); ?> /></td>
                </tr>
            </table>
        </div>
        <?php
    }
    public function submenu_tab_style(){
        ?>
        <div id="acwp_style" class="acwp-tab">
            <h2><?php _e('Style', 'acwp');?></h2>

            <table class="form-table" id="acwp-styling">
                <tr valign="top">
                    <th scope="row"><?php _e('Toolbar Side', 'acwp');?></th>
                    <td>
                        <select name="acwp_toolbar_side" id="acwp_toolbar_side">
                            <option value=""><?php _e('Left (default)', 'acwp');?></option>
                            <option value="right" <?php selected('right', get_option('acwp_toolbar_side'))?>><?php _e('Right', 'acwp');?></option>
                        </select>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row"><?php _e('Use custom colors for toolbar?', 'acwp');?></th>
                    <td><input type="checkbox" name="acwp_toolbar_customcolors" id="acwp_toolbar_customcolors" value="yes" <?php checked( esc_attr( get_option('acwp_toolbar_customcolors') ), 'yes' ); ?> /></td>
                </tr>

                <tr valign="top" class="acwp-hide-row" id="acwp-heading-bgcolor-row">
                    <th scope="row"><?php _e('Heading Background Color', 'acwp');?></th>
                    <td><input type="color" name="acwp_heading_bg" class="color-field" value="<?php echo esc_attr( get_option('acwp_heading_bg') ); ?>" data-default-color="#3c8dbc" /></td>
                </tr>

                <tr valign="top" class="acwp-hide-row" id="acwp-heading-txtcolor-row">
                    <th scope="row"><?php _e('Heading Text Color', 'acwp');?></th>
                    <td><input type="color" name="acwp_heading_textcolor" class="color-field" value="<?php echo esc_attr( get_option('acwp_heading_textcolor') ); ?>" data-default-color="#ffffff" /></td>
                </tr>

                <tr valign="top" class="acwp-hide-row" id="acwp-heading-lnkcolor-row">
                    <th scope="row"><?php _e('Links Color', 'acwp');?></th>
                    <td><input type="color" name="acwp_linkscolor" class="color-field" value="<?php echo esc_attr( get_option('acwp_linkscolor') ); ?>" data-default-color="#3c8dbc" /></td>
                </tr>

                <tr>
                    <th colspan="2">
                        <h3 style="font-size: 16px;"><?php _e('Toggle Button', 'acwp');?></h3>
                    </th>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Position from top', 'acwp');?></th>
                    <td><input type="number" name="acwp_toggle_fromtop" id="acwp_toggle_fromtop" value="<?php echo esc_attr( get_option('acwp_toggle_fromtop') );?>" />px</td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Position from side', 'acwp');?></th>
                    <td><input type="number" name="acwp_toggle_fromside" id="acwp_toggle_fromside" value="<?php echo esc_attr( get_option('acwp_toggle_fromside') );?>" />px</td>
                </tr>
            </table>
        </div>
        <?php
    }
    public function submenu_tab_additional(){
        ?>
        <div id="acwp_additional" class="acwp-tab">
            <h2><?php _e('Additional Links', 'acwp');?></h2>
            <h3><?php _e('Accessibility Statement', 'acwp');?></h3>

            <table class="form-table">
                <tr valign="top">
                    <th scope="row"><?php _e('Label', 'acwp');?></th>
                    <td><input type="text" name="acwp_statement_label" value="<?php echo esc_attr( get_option('acwp_statement_label') ); ?>" placeholder="<?php _e('Accessibility Statement', 'acwp');?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Link', 'acwp');?></th>
                    <td><input type="url" name="acwp_statement" value="<?php echo esc_attr( get_option('acwp_statement') ); ?>" placeholder="http://" /></td>
                </tr>
            </table>

            <h3><?php _e('Send Feedback', 'acwp');?></h3>

            <table class="form-table">
                <tr valign="top">
                    <th scope="row"><?php _e('Label', 'acwp');?></th>
                    <td><input type="text" name="acwp_feedback_label" value="<?php echo esc_attr( get_option('acwp_feedback_label') ); ?>" placeholder="<?php _e('Send Feedback', 'acwp');?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('Link', 'acwp');?></th>
                    <td><input type="url" name="acwp_feedback" value="<?php echo esc_attr( get_option('acwp_feedback') ); ?>" placeholder="http://" /></td>
                </tr>
            </table>
        </div>
        <?php
    }

    public function submenu_page_callback() {
        ?>
        <div id="accessible-wp-toolbar" class="wrap">
            <h1><?php _e('AccessibleWP Accessibility Toolbar', 'acwp');?></h1>
            <form method="post" action="options.php">
                <?php settings_fields( 'acwp' ); ?>
                <?php do_settings_sections( 'acwp' ); ?>

                <div class="nav-tab-wrapper">
                    <a href="#acwp_heading" class="nav-tab nav-tab-active"><?php _e('Heading', 'acwp');?></a>
                    <a href="#acwp_options" class="nav-tab"><?php _e('Options', 'acwp');?></a>
                    <a href="#acwp_additional" class="nav-tab"><?php _e('Additional Links', 'acwp');?></a>
                    <a href="#acwp_settings" class="nav-tab"><?php _e('Settings', 'acwp');?></a>
                    <a href="#acwp_style" class="nav-tab"><?php _e('Style', 'acwp');?></a>
                </div>
                <?php
                echo $this->submenu_tab_heading();
                echo $this->submenu_tab_options();
                echo $this->submenu_tab_additional();
                echo $this->submenu_tab_settings();
                echo $this->submenu_tab_style();
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }
}
if( is_admin() )
    new ACWP_AdminPanel();