=== AccessibleWP Toolbar - WordPress Accessibility Toollbar ===

Contributors: digisphere, everaccess, codenroll
Tags: Accessibility, WCAG, a11y, WAI, Aria, Contrast, Font Size, Access, Toolbar, Toolkit, Tabindex, user1, WordPress Accessibility, Accessible, WP Accessibility, Aaccessibility, נגישות, הנגשת אתר, נגישות אתרים
Requires at least: 4.3
Tested up to: 5.4.2
Stable tag: 4.0.4
Version: 4.0.4
License: GPLv2
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==
The AccessibleWP Toolbar provides a menu with a set of options that help make your site accessible to its users.

== Toolbar Options ==
 * Keyboard Navigation - allow navigate through keyboard and enhance keyboard functionality.
 * Disable Animation - allow the user to disable CSS3 animations.
 * Dark Contrast - let the user change site colors to colors with dark contrast (also let you choose the colors).
 * Monochrome - let the user change site colors to grayscale colors.
 * Change Font Size - allow the user increase or decrease the font size on your website.
 * Readable Font - let the user change the font-family to more readable font (also let you choose which font is the readable font).
 * Mark Titles - allow the user to mark the titles on your site.
 * Mark Links - allow the user to mark the links on your site.
 * Links Underline - allow the user to mark all links with underline.

This plugin is provided by Amit Moreno and the team of Code n' Roll to help make the internet more accessible for people with disabilities. please aware that the plugin authors is not responsible to the user or to any third party for any direct or indirect damage of any kind from any use of this plugin.

== Installation ==

1. Download the link and upload the zip file to your plugins folder, or search for it on WordPress plugins.
2. Activate the plugin.
4. Go to the "Toolbar" admin page under the new "Accessibility" admin menu item and follow the instructions there.

== Changelog ==

= 1.0.1 =
* Adding screenshots
* Fixing jQuery issue with the Font-Sizer script
* Add a cookie so the browser will remember if the user press on one of the Contrast options

= 1.0.2 =
* The Skiplinks moved below the Fixed Toolbar so the user could land first on them
* A new option was added to the Skiplinks that gives the ability to use different links for the Skiplinks on the Home Page.
* Fixing the shortcode so they will be return at the exact place.
* Some CSS Styles was given to the shorted buttons.
* The plugin was translated to Hebrew.

= 1.2.1 =
* remove shortcodes.
* add option to use attachment title in images ALT who don't have an alt.

= 1.2.2 =
* add option to affect the whole site with the contrast modes
* hide the toolbar when user scroll


= 1.2.3 =
* fix the z-index of the toolbar
* fix the text appear below the small icon
* add option to use attachment description in images ALT who don't have an alt
* changing toolbar tabindex when it's close or open
* improving skiplinks script
* improve toolbar as ajax
* improve skiplinks as ajax

= 1.2.4 =
* The design of the toolbar changed
* Added new option to disable toolbar buttons animations
* Readable font button was added to the toolbar
* All toolbar buttons now have icons
* The option to affect everything with the Contrast become default and the button for it removed
* The option to remove Skiplinks buttons underline removed
* The Skiplinks style changed
* fixed issue with keyboard navigation

= 1.2.5 =
* Fix font-size issue
* Added buttons to set the minimum and the maximum for the increment & the decrement font-size buttons

= 1.3.0 =
* minify js
* code improvement
* Included Images missing ALT's platform
* The z-index of the toolbar got higher
* a Conflict with Jetpack fixed
* a Conflict with Contact form 7 fixed
* The readable font option improved
* aria-hidden rules added to the accessibility toolbar
* Make elements linked with the skiplinks focusable

= 1.3.1 =
* Remove the button to affect all elements with the font-size
* add defaults for the font-size buttons
* Improve toolbar styling for LTR users
* Add Feedback button to the toolbar with options to hide it and to change the Text & the URL  address.
* Add Accessibility declaration button to the toolbar with options to hide it and to change the Text & the URL  address.

= 1.3.2 =
Update assets admin name to match Avada theme.

= 1.3.3 =
* Add option to disable the toolbar.
* Add option to hide toolbar on mobile.
* Fix tbindex issue of toolbar inner links.

= 2.0 =
* core change
* more options added

= 2.1.1 =
* New Option: Choose 1 of 2 different skins for the Toolbar
* New Option: Change the side of the Toolbar
* New Option: Change the size of the Toolbar icon
* New Option: Change the position of the Toolbar icon
* New Option: Replace HTML tags with other tags and keep attributes (up to 3 replacement tags)
* New Option: Add extra information to the logo with ARIA-LABEL attribute
* New Option: Remove unnecessary tabindex attributes
* Restore Option: Load Toolbar with AJAX + Add more validation for the process
* Restore Option: Load Skiplinks with AJAX + Add more validation for the process
* New Option: Change the side of the Skiplinks
* Option removed: Add Tabindex to heading tags
* Option replaced: Change the TITLE attribute to ARIA-LABEL or the add extra ARIA-LABEL attribute with the value of the TITLE attribute is added instead of the option to only replace the TITLE attribute
* Redesign options panel
* Code performance

= 3.0.1 =
* Code improvement

= 3.0.2 =
* Added the ability to include and exclude objects with the font size modifier.
* Changed the use of the class "label" to "acp-label" to avoid conflicts.
* Fixed issue of icons with the Readable button of the toolbar.
* Improved the design of the toolbar.
* New toolbar skin: Smooth.
* Add more ARIA support for the toolbar buttons.

= 4.0.0 =
* An entirely new version where React replace the old php templates.
* All options that unrelated to the toolbar were removed from this plugin and launched in separate plugins.
* All saved settings from previous version will be reset.  

= 4.0.2 =
*   Fix route issue
