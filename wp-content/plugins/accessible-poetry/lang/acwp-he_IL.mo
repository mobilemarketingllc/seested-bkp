��    V      �     |      x     y     �     �  !   �  A   �  #     +   =     i  :   z     �     �  .   �  .   	     <	  "   I	  "   l	     �	  -   �	  4   �	     
  
   
     )
     2
     @
     S
     a
     t
     �
  '   �
  ,   �
     �
  /     .   7      f     �     �     �     �     �     �     �     �             !        <     ?     S     Y     h     m     �     �  
   �     �     �  
   �     �     �     �     �               0     >     D     R  #   [          �  
   �  �   �     h     �     �     �     �     �     �     �     �            "   *     M  U  f     �     �     �  %   �  U     J   q  ]   �       D   6     {      �  3   �  3   �        8   5  A   n     �  :   �  R   
     ]     {     �     �     �     �     �          2  9   N  :   �     �  H   �  \   ,  !   �  .   �     �     �     �  
             -     C     Y     f  ,   �  	   �     �  
   �     �     �          1      I     j     �     �     �  )   �     �  "   �          "     6     N     `     i     }  D   �  
   �     �     �     �     �          3     P     m     �     �  1   �  J   �  2   -  4   `  >   �  +   �     4           .   "   '                     =   N         U   (       F   E   M   *   5   7   #          ?          T      /                  :           +   Q         @      <                     L   2   G      K   0       %              3       I   &   8   9          D              >   -   R             P   )           B   S   
   $                     O      6   J   !   	      A                                        ;           ,       C   H   V   1                      Accessibility Accessibility Statement Accessibility Toolbar Accessibility Toolbar Toggle View Add custom background color and text color to heading components. Add line under all link components. Add red border to components in focus mode. Additional Links Allow navigation between components using keyboard arrows. Backgrounds Color Black & White Contrast By how many percent to decrease the font size? By how many percent to increase the font size? Change Label Change Label of Decrease Text Size Change Label of Increase Text Size Change Text Size Change the colors of everything to grayscale. Change the font family of all text to readable font. Choose your readable font: Contact us Contrast Custom Family Custom Font Family Dark Contrast Decrease Text Size Default (Arial) Disable Animations Disable CSS3 Animations with CSS class. Disable arrows navigation between components Disable background images. Disable line-height reset when size is changing Disable red border to components in focus mode Do not disable background images Hard CSS Heading Heading Background Color Heading Color Heading Description Heading Text Color Heading Title Hide Option Images Increase Text Size Increase and decrease font sizes. JS Keyboard Navigation Label Left (default) Link Links Background Color Links Color Links Text Color Mark Links Mark Titles Mode Monochrome Normal CSS (default) Options Our accessibility plugins Our website Position from side Position from top Readable Font Right Send Feedback Settings Show toolbar also on mobile devices Style Text Text Color This plugin was developed as part of our Accessibility plugins package to allow as much accessibility as possible to users across the network. we offer a variety of Accessibility plugins as you will find below. Titles Background Color Titles Text Color Toggle Button Toolbar Toolbar Heading Toolbar Side Underline Links Use custom colors Use custom colors for toolbar? Use custom colors? Use custom tags? Welcome to AccessibleWP Dashboard! separate tags with comma Project-Id-Version: 
POT-Creation-Date: 2020-04-14 23:45+0300
PO-Revision-Date: 2020-04-14 23:59+0300
Last-Translator: 
Language-Team: 
Language: he_IL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
X-Poedit-Basepath: .
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n==2 ? 1 : n>10 && n%10==0 ? 2 : 3);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: assets
X-Poedit-SearchPathExcluded-1: package.json
X-Poedit-SearchPathExcluded-2: webpack.config.js
X-Poedit-SearchPathExcluded-3: config.rb
 נגישות הצהרת נגישות כלי נגישות הצגת סרגל כלי נגישות הגדר צבע רקע וצבע טקסט עבור רכיב סימון הכותרות. הוספת קו תחתי מתחת לכל רכיבי הקישור באתר. הוספת קו תוחם בצבע אדום לרכיבים הנמצאים במצב פוקוס. קישורים נוספים אפשר ניווט בין רכיבים באמצעות המקלדת. צבע הרקע ניגודיות שחור-לבן בכמה אחוזים להקטין את הגופן? בכמה אחוזים להגדיל את הגופן? שינוי טקסט התווית שינוי התווית להקטנת גודל הגופן שינוי תווית הלחצן להגדלת גודל הגופן שינוי גודל הטקסט שינוי כלל הצבעים לגוונים אפורים שינוי משפחת הגופנים של הטקסט באתר לגופן קריא. בחירת גופן קריא: יצירת קשר ניגודיות גופן מותאם אישית גופן מותאם אישית ניגודיות כהה הקטנת גודל הגופן Arial (ברירת מחדל‫) ביטול אנימציות ביטול אנימציות CSS3 באמצעות קלאס. ביטול ניווט חצים באמצעות המקלדת ביטול תמונות רקע. ביטול איפוס גובה השורה כאשר הגופן משתנה ביטול קו תוחם אדום עבור רכיםבים הנמצאים במצב פוקוס אל תבטל תמונות רקע מצב CSS מחוזק (בתוספת important) כותר צבע רקע הכותר צבע הכותר תיאור טקסט הכותר כותרת ראשית הסתר אפשרות תמונות הגדלת גופן גופן הגדלה והקטנת גודל הגופן. מצב JS ניווט מקלדת תווית שמאל (ברירת מחדל) כתובת הקישור צבע רקע הקישורים צבע הקישורים צבע טקסט הקישורים סימון כותרות סימון כותרות מצב מונוכרום מצב CSS רגיל (ברירת מחדל) אפשרויות תוספי הנגישות שלנו האתר שלנו מיקום מהצד מיקום מלמעלה גופן קריא ימין שליחת משוב הגדרות הצג את כלי הנגישות גם למכשירים ניידים סגנון טקסט צבע הטקסט   צבע רקע הכותרות צבע טקסט הכותרות לחצן הצגת הסרגל סרגל כלי נגישות כותר הסרגל צד הסרגל קו תחתי לקישורים השתמש בצבעים מותאמים אישית להשתמש בצבעים מותאמים אישית לסרגל הכלים? להשתמש בצבעים מותאים אישית? להשתמש בתגיות מותאמות אישית? ברוכים הבאים ללוח הבקרה של AccessibleWP! הפרד תגיות באמצעות פסיק 