function acwp_hasClass(ele,cls) {
    return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}
if( acwp_hasClass(document.getElementsByTagName('body')[0], 'accessiblewp_page_accessiblewp-toolbar') ){
    document.getElementById('acwp_readable_font').onchange = function() {
        var index = this.selectedIndex;
        var inputVal = this.children[index].value;

        if( inputVal == 'custom' )
            document.getElementById('acwp-row-readable-custom').style.display = 'table-row';
        else
            document.getElementById('acwp-row-readable-custom').style.display = 'none';
    }
    document.getElementById('acwp_contrast_custom').onclick = function() {

        if( this.checked == true ) {
            document.getElementById('acwp-contrast-bgcolor-row').style.display = 'table-row';
            document.getElementById('acwp-contrast-txtcolor-row').style.display = 'table-row';
            document.getElementById('acwp-contrast-linkscolor-row').style.display = 'table-row';
        }
        else {
            document.getElementById('acwp-contrast-bgcolor-row').style.display = 'none';
            document.getElementById('acwp-contrast-txtcolor-row').style.display = 'none';
            document.getElementById('acwp-contrast-linkscolor-row').style.display = 'none';
        }
    }
    if( document.getElementById('acwp_contrast_custom').checked == true ) {
        document.getElementById('acwp-contrast-bgcolor-row').style.display = 'table-row';
        document.getElementById('acwp-contrast-txtcolor-row').style.display = 'table-row';
        document.getElementById('acwp-contrast-linkscolor-row').style.display = 'table-row';
    }

    document.getElementById('acwp_fontsize_customtags').onclick = function() {

        if( this.checked == true ) {
            document.getElementById('acwp-fontsize-tags-row').style.display = 'table-row';
        }
        else {
            document.getElementById('acwp-fontsize-tags-row').style.display = 'none';
        }
    }
    if( document.getElementById('acwp_fontsize_customtags').checked == true ) {
        document.getElementById('acwp-fontsize-tags-row').style.display = 'table-row';
    }

    document.getElementById('acwp_titles_customcolors').onclick = function() {

        if( this.checked == true ) {
            document.getElementById('acwp-titles-bg-row').style.display = 'table-row';
            document.getElementById('acwp-titles-txt-row').style.display = 'table-row';
        }
        else {
            document.getElementById('acwp-titles-bg-row').style.display = 'none';
            document.getElementById('acwp-titles-txt-row').style.display = 'none';
        }
    }
    if( document.getElementById('acwp_titles_customcolors').checked == true ) {
        document.getElementById('acwp-titles-bg-row').style.display = 'table-row';
        document.getElementById('acwp-titles-txt-row').style.display = 'table-row';
    }

    document.getElementById('acwp_toolbar_customcolors').onclick = function() {

        if( this.checked == true ) {
            document.getElementById('acwp-heading-bgcolor-row').style.display = 'table-row';
            document.getElementById('acwp-heading-txtcolor-row').style.display = 'table-row';
            document.getElementById('acwp-heading-lnkcolor-row').style.display = 'table-row';
        }
        else {
            document.getElementById('acwp-heading-bgcolor-row').style.display = 'none';
            document.getElementById('acwp-heading-txtcolor-row').style.display = 'none';
            document.getElementById('acwp-heading-lnkcolor-row').style.display = 'none';
        }
    }
    if( document.getElementById('acwp_toolbar_customcolors').checked == true ) {
        document.getElementById('acwp-heading-bgcolor-row').style.display = 'table-row';
        document.getElementById('acwp-heading-txtcolor-row').style.display = 'table-row';
        document.getElementById('acwp-heading-lnkcolor-row').style.display = 'table-row';
    }

    document.getElementById('acwp_links_customcolors').onclick = function() {

        if( this.checked == true ) {
            document.getElementById('acwp-links-bgcolor-row').style.display = 'table-row';
            document.getElementById('acwp-links-txtcolor-row').style.display = 'table-row';
        }
        else {
            document.getElementById('acwp-links-bgcolor-row').style.display = 'none';
            document.getElementById('acwp-links-txtcolor-row').style.display = 'none';
        }
    }
    if( document.getElementById('acwp_links_customcolors').checked == true ) {
        document.getElementById('acwp-links-bgcolor-row').style.display = 'table-row';
        document.getElementById('acwp-links-txtcolor-row').style.display = 'table-row';
    }

    jQuery(document).ready(function($){

        $('#accessible-wp-toolbar .nav-tab').click(function (e) {
            e.preventDefault();
            var tab = $(this).attr('href');

            $('.acwp-tab').each(function () {
                $(this).removeClass('active');
            });

            $(tab).addClass('active');

            $('.nav-tab').each(function () {
                $(this).removeClass('nav-tab-active');
            });

            $(this).addClass('nav-tab-active');
        });

        // Activate wp color picker
        $('.color-field').each(function(){
            $(this).wpColorPicker();
        });

        if( $('#acwp_readable_font :selected').val() == 'custom' )
            document.getElementById('acwp-row-readable-custom').style.display = 'table-row';
    });
}