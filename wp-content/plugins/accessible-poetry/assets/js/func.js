function acwp_hasClass(ele,cls) {
    return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}

jQuery(document).ready(function ($) {

    setTimeout(function () {
        var btn = $('#acwp-toolbar-btn');
        // var top  = btn.data('top');
        var side = btn.data('side');
        var style = {
            // top : top,
            //eft: side
        };

        if( btn.hasClass('acwp-right') ) {
            style = {
                //top : top,
                //right: side
            };
        }
        $('#acwp-toolbar-btn').css( style );
    }, 250);
    setTimeout(function () {
        $('#acwp-toolbar-btn').addClass('show');
    }, 1500);

    // ?
    jQuery( "#acwp-toolbar .acwp-toggler label" ).keypress(function( event ) {
        if ( event.which == 13 ) {
            event.preventDefault();
            jQuery(this).click();
        }
    });



    jQuery( "#acwp-toolbar-btn" ).on('mousedown', function (e) {
        e.preventDefault();
        window.my_dragging = {};
        my_dragging.pageX0 = e.pageX;
        my_dragging.pageY0 = e.pageY;
        my_dragging.elem = this;
        my_dragging.offset0 = jQuery(this).offset();
        function handle_dragging(e){
            var top = my_dragging.offset0.top + (e.pageY - my_dragging.pageY0);
            $(my_dragging.elem)
                .offset({top: top});
        }
        function handle_mouseup(e){
            jQuery('body')
                .off('mousemove', handle_dragging)
                .off('mouseup', handle_mouseup);
        }
        jQuery('body')
            .on('mouseup', handle_mouseup)
            .on('mousemove', handle_dragging);
    });
});