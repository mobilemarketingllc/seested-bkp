import React from 'react';
import ReactDOM from 'react-dom';
import MainWrap from './components/mainWrap';

const toolbar = document.getElementsByClassName('acwp-toolbar');

if( toolbar.length ) {
    ReactDOM.render(
        <MainWrap />,
        toolbar[ 0 ]
    );
}