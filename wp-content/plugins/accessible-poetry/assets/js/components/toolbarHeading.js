import React from 'react';

export default class ToolbarHeading extends React.Component {
    constructor(props) {
        super(props);
        this.closeToolbar = this.closeToolbar.bind(this);
    }

    closeToolbar() {
        document.getElementById('acwp-toolbar').classList.remove('acwp-toolbar-active');
        document.getElementById('acwp-toolbar-btn').style.display = 'block';
    }

    render() {
        let heading, desc;

        let bgcolor = this.props.customcolors == 'yes' ? this.props.bgcolor : '';
        let textcolor = this.props.customcolors == 'yes' ? this.props.textcolor : '';

        if( this.props.heading ) {
            heading = <h1>{this.props.heading}</h1>;
        }
        if( this.props.desc ) {
            desc = <p>{this.props.desc}</p>;
        }

        if(this.props.heading || this.props.desc) {
            return (
                <div className="acwp-heading" style={{backgroundColor: bgcolor, color: textcolor}}>
                    {heading}
                    {desc}
                    <button type="button" id="acwp-close-toolbar" onClick={this.closeToolbar}><i className="material-icons">close</i></button>
                </div>
            );
        }
        else {
            return (
                <button type="button" id="acwp-close-toolbar" onClick={this.closeToolbar}><i className="material-icons">close</i></button>
            );
        }
    }
}