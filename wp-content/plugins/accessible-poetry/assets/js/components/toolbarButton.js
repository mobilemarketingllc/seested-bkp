import React from 'react';
import { Icon, InlineIcon } from '@iconify/react';
import universalAccess from '@iconify/icons-dashicons/universal-access';

export default class ToolbarButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            results : [],
        };

    }
    componentDidMount() {
        fetch(acwp_rest.acwp_settings, {headers: {'X-WP-Nonce': acwp_rest.nonce}})
            .then(response => {
                return response.json();
            })
            .then(results => {
                this.setState({results: results});
            });
    }

    openToolbar() {
        document.getElementById('acwp-toolbar-btn').style.display = 'none';
        document.getElementById('acwp-toolbar').classList.add('acwp-toolbar-active');
        document.getElementById('acwp-toolbar').focus();
    }
    render() {
        let buttonClass = '';
        if( document.getElementById('acwp-toolbar').classList.contains('acwp-right') )
            buttonClass = 'acwp-right';

        let top = this.state.results.toggle_fromtop ? this.state.results.toggle_fromtop : 10;
        let side = this.state.results.toggle_fromside ? this.state.results.toggle_fromside : 10;

        let buttonStyle = {
            top: top + 'px',
            left: side + 'px',
        };

        if( this.state.results.direction_mode == 'right' ){
            buttonStyle = {
                top: top + 'px',
                right: side + 'px',
            };
        }
        return (
            <button type="button" style={buttonStyle} className={buttonClass} id="acwp-toolbar-btn" onClick={this.openToolbar} tabIndex="0" aria-label="Toggle Accessibility Toolbar" data-side={side}>
                <Icon icon={universalAccess} />
            </button>
        );

    }
}