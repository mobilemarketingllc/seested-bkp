import React from 'react';
import Toolbar from './toolbar';
import ToolbarButton from './toolbarButton';

export default class MainWrap extends React.Component {
    constructor(props) {
        super(props);
        this.openToolbar = this.openToolbar.bind(this);
    }

    openToolbar() {
        document.getElementById('acwp-toolbar').classList.add('acwp-toolbar-active');
        document.getElementById('acwp-toolbar-btn').style.display = 'none';
    }

    render() {
        setTimeout(function () {
            jQuery('body').prepend( jQuery('#acwp-toolbar-btn') );
        }, 1200);

        return (
            <div>
                <Toolbar />
                <ToolbarButton onClick={this.openToolbar} />
            </div>
        )
    }
}