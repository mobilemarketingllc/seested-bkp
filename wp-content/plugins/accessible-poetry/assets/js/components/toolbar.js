import React from 'react';
import Toggler from './toggler';
import ToolbarHeading from './toolbarHeading';

export default class Toolbar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            results : [],
        };
    }

    componentWillMount() {

        // Get Settings
        fetch(acwp_rest.acwp_settings, {
            headers: {'X-WP-Nonce': acwp_rest.nonce}
        })
            .then(response => { return response.json(); })
            .then(results => {
                this.setState({results: results});
            });
    }

    render() {
        let buttonKetboard, buttonContrast, buttonMonochrome,
            buttonAnimations, headingContrast, buttonsFontSize,
            buttonReadable, buttonTitles, buttonLinks, buttonUnderine,
            headingText, statement, feedback, more;

        const buttonContrastHide    = this.state.results.contrast_hide;
        const buttonMonochromeHide  = this.state.results.monochrome_hide;
        const buttonsFontSizeHide   = this.state.results.fontsize_hide;
        const buttonReadableHide    = this.state.results.readable_hide;
        const buttonTitlesHide      = this.state.results.titles_hide;
        const buttonLinksHide       = this.state.results.links_hide;
        const buttonUnderineHide    = this.state.results.underline_hide;

        let bottom = <li>Powered with <i className="material-icons">favorite</i> by <a href="https://www.codenroll.co.il/" style={{color: this.state.results.linkscolor}}>Code n' Roll</a></li>;

        // Implement the bottom copyrights links
        if( jQuery('body').hasClass('rtl') )
            bottom = <li>מופעל ב <i className="material-icons">favorite</i> ע״י <a href="https://www.codenroll.co.il/" style={{color: this.state.results.linkscolor}}>קוד אנד רול</a></li>;
        else
            bottom = <li>Powered with <i className="material-icons">favorite</i> by <a href="https://www.codenroll.co.il/" style={{color: this.state.results.linkscolor}}>Code n' Roll</a></li>;

        // more ?
        if( this.state.results.animations_hardmode == 'yes' )
            more = 'hardmode';
        else if( this.state.results.contrast_mode == 'js' )
            more = 'js';

        // Button: Keyboard navigation
        if(
            this.state.results.keyboard_hide != 'yes' &&
            (this.state.results.keyboard_noborder != 'yes' || this.state.results.keyboard_noarrows != 'yes')
        ) {
            buttonKetboard = <Toggler name="keyboard" label={this.state.results.keyboard_label} />;
        }

        // Button: Contrast
        if( buttonContrastHide != 'yes' ) {
            buttonContrast = <Toggler name="contrast" label={this.state.results.contrast_label} more={more} />;
        }

        // Button: monochrome
        if( buttonMonochromeHide != 'yes' ) {
            buttonMonochrome = <Toggler name="monochrome" label={this.state.results.monochrome_label} />;
        }

        // Button: Animations
        if( this.state.results.animations_hide != 'yes' ) {
            buttonAnimations = <Toggler name="animations" label={this.state.results.animations_label} more={more} />;
        }

        // Buttons: Font size
        if( buttonsFontSizeHide != 'yes' ) {
            let incsize = this.state.results.incfont_size;
            let decsize = this.state.results.decfont_size;
            let nolineheight = this.state.results.fontsize_nolineheight;
            let customtags = (this.state.results.fontsize_customtags == 'yes') ? this.state.results.fontsize_tags : 'p,h1,h2,h3,h4,h5,h6,label';
            buttonsFontSize = <div>
                <Toggler name="incfont" label={this.state.results.incfont_label} more={{incsize, customtags, nolineheight}} />
                <Toggler name="decfont" label={this.state.results.decfont_label} more={{decsize, customtags, nolineheight}} />
            </div>
        }

        // Button: Readable
        if( buttonReadableHide != 'yes' ) {
            buttonReadable = <Toggler name="readable" label={this.state.results.readable_label} />;
        }

        // Button: Titles
        if( buttonTitlesHide != 'yes' ) {
            buttonTitles = <Toggler name="marktitles" label={this.state.results.titles_label} />;
        }

        // Button:
        if( buttonLinksHide != 'yes' ) {
            buttonLinks = <Toggler name="marklinks" label={this.state.results.links_label} />;
        }
        if( buttonUnderineHide != 'yes' ) {
            buttonUnderine = <Toggler name="underline" label={this.state.results.underline_label} />;
        }


        if( buttonMonochromeHide != 'yes' || buttonContrastHide != 'yes' ) {
            headingContrast = <h2>{this.state.results.contrast_heading}</h2>;
        }
        if( buttonsFontSizeHide != 'yes' || buttonReadableHide != 'yes' || buttonTitlesHide != 'yes' || buttonLinksHide != 'yes' || buttonUnderineHide != 'yes') {
            headingText = <h2>{this.state.results.text_heading}</h2>;
        }

        if( this.state.results.heading_bg ) {
            //document.getElementsByClassName('acwp-heading').style.backgroundColor = this.state.results.heading_bg;
        }

        if( this.state.results.statement && this.state.results.statement != '' ){
            statement = <li><a href={this.state.results.statement} style={{color: this.state.results.linkscolor}}>{this.state.results.statement_label}</a></li>;
        }

        if( this.state.results.feedback && this.state.results.feedback != '' ){
            feedback = <li><a href={this.state.results.feedback} style={{color: this.state.results.linkscolor}}>{this.state.results.feedback_label}</a></li>;
        }

        return (
            <div id="acwp-toolbar-module">
                <ToolbarHeading closeToolbar={this.props.closeToolbar} customcolors={this.state.results.toolbar_customcolors} heading={this.state.results.heading_title} desc={this.state.results.heading_desc} bgcolor={this.state.results.heading_bg} textcolor={this.state.results.heading_textcolor} />
                <div className="acwp-togglers">
                    {buttonKetboard}
                    {buttonAnimations}

                    {headingContrast}

                    {buttonContrast}
                    {buttonMonochrome}

                    {headingText}
                    {buttonsFontSize}
                    {buttonReadable}


                    {buttonTitles}
                    {buttonLinks}
                    {buttonUnderine}
                </div>
                <div className="acwp-footer">
                    <ul>
                        {statement}
                        {feedback}
                        {bottom}
                    </ul>
                </div>
            </div>
        );

    }
}