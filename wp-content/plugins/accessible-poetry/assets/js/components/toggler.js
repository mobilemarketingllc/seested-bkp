import React from 'react';
import { Icon, InlineIcon } from '@iconify/react';
import universalAccess from '@iconify/icons-dashicons/universal-access';

export default class Toggler extends React.Component {
    constructor(props) {
        super(props);
    }

    /**
     * Handle Click
     *
     * @param e - Event
     * @param name - Button name
     */
    labelClick(e, name) {

        let bodyClassEvents = ['monochrome', 'underline', 'marklinks', 'marktitles', 'readable', 'animations'];

        // If our button is one of the list above:
        if( bodyClassEvents.includes(name) ) {

            // get checkbox
            var checkbox = document.getElementById('acwp-toggler-' + name);

            // Toggle Option
            if( checkbox.checked == true && !acwp_hasClass(document.body, 'acwp-' + name) ) {
                document.body.classList += ' acwp-' + name;
            }
            else {
                document.body.classList.remove('acwp-' + name);
            }
        }
        else if( name == 'incfont' ){

            // Add body class
            jQuery('#acwp-toolbar').addClass('incresed');

            // get our custom tags if there's
            let customTags = this.props.more['customtags'];

            // set the font size distance
            let newSize = (this.props.more['incsize'] && this.props.more['incsize'] != '') ? this.props.more['incsize'] / 100 : 1.6;

            // get line height optioon
            let noLineHeight = this.props.more['nolineheight'];

            // get increase checkbox
            let checkbox = document.getElementById('acwp-toggler-incfont');

            // get decrease checkbox
            let decfontCheckbox = document.getElementById('acwp-toggler-decfont');

            // if decrease checkbox is checked turn it off
            if( decfontCheckbox.checked == true )
                decfontCheckbox.checked = false;

            // if increase checkbox is on
            if( checkbox.checked == true ) {

                // Change font size
                jQuery(customTags).each(function () {
                    let fontSize = jQuery(this).css('font-size');
                    if( !jQuery(this).data('acwp-fontsize') )
                        jQuery(this).attr('data-acwp-fontsize', fontSize.substring(0, fontSize.length - 2));
                    jQuery(this).css('font-size', parseInt(fontSize) * newSize + 'px');

                    if(noLineHeight != 'yes'){
                        let lineHeight = jQuery(this).css('line-height');
                        if( !jQuery(this).data('acwp-lineheight') )
                            jQuery(this).attr('data-acwp-lineheight', lineHeight.substring(0, lineHeight.length - 2));
                        jQuery(this).css('line-height', 'normal');
                    }
                });
            }
            else {
                let noLineHeight = this.props.more['nolineheight'];

                jQuery(customTags).each(function () {
                    let fontSize = jQuery(this).data('acwp-fontsize');
                    jQuery(this).css('font-size', parseInt(fontSize) + 'px');

                    if(noLineHeight != 'yes'){
                        let lineHeight = jQuery(this).data('acwp-lineheight');
                        jQuery(this).css('line-height', parseInt(lineHeight) + 'px');
                    }
                });
            }
        }
        else if( name == 'decfont' ){
            let noLineHeight = this.props.more['nolineheight'];
            let customTags = this.props.more['customtags'];
            let newSize = (this.props.more['decsize'] && this.props.more['decsize'] != '') ? this.props.more['decsize'] / 100 : 0.8;
            let incfontCheckbox = document.getElementById('acwp-toggler-incfont');

            if( incfontCheckbox.checked == true ) {
                incfontCheckbox.checked = false;
            }

            var checkbox = document.getElementById('acwp-toggler-decfont');
            if( checkbox.checked == true ) {

                jQuery(customTags).each(function () {
                    let fontSize = jQuery(this).css('font-size');

                    if( !jQuery(this).data('acwp-fontsize') )
                        jQuery(this).attr('data-acwp-fontsize', fontSize.substring(0, fontSize.length - 2));
                    jQuery(this).css('font-size', parseInt(fontSize) * newSize + 'px');

                    if(noLineHeight != 'yes'){
                        let lineHeight = jQuery(this).css('line-height');
                        if( !jQuery(this).data('acwp-lineheight') )
                            jQuery(this).attr('data-acwp-lineheight', lineHeight.substring(0, lineHeight.length - 2));
                        jQuery(this).css('line-height', 'normal');
                    }
                });
            }
            else {
                jQuery(customTags).each(function () {
                    let fontSize = jQuery(this).data('acwp-fontsize');
                    jQuery(this).css('font-size', parseInt(fontSize) + 'px');

                    if(noLineHeight != 'yes'){
                        let lineHeight = jQuery(this).data('acwp-lineheight');
                        jQuery(this).css('line-height', parseInt(lineHeight) + 'px');
                    }
                });
            }
        }
        else if( name == 'keyboard' ){
            var checkbox = document.getElementById('acwp-toggler-keyboard');
            if( checkbox.checked == true && !acwp_hasClass(document.body, 'acwp-keyboard') ) {
                document.body.classList += ' acwp-keyboard';
            }
            else {
                document.body.classList.remove('acwp-keyboard');
            }
        }
        else if( name == 'contrast' ){
            var checkbox = document.getElementById('acwp-toggler-contrast');

            if( checkbox.checked == true && !acwp_hasClass(document.body, 'acwp-contrast') ) {
                document.body.classList += ' acwp-contrast';

                if( acwp_hasClass(document.body, 'acwp-contrast-js') ) {
                    // imgs
                    jQuery('body *').each(function () {
                        if( this.style.backgroundImage != '' )
                            jQuery(this).attr('data-acwp-bgimage', this.style.backgroundImage);
                        this.style.backgroundImage = 'none';
                    });
                    // bgs
                    jQuery('body *').each(function () {
                        if( this.style.backgroundColor != '' )
                            jQuery(this).attr('data-acwp-bgcolor', this.style.backgroundColor);
                        this.style.backgroundColor = 'black';
                    });
                    // txt
                    jQuery('body *').each(function () {
                        if( this.tagName == 'A' || this.tagName == 'BUTTON' || this.tagName == 'LABEL' ){
                            if( this.style.color != '' )
                                jQuery(this).attr('data-acwp-lnkcolor', this.style.color);
                            this.style.color = 'yellow';
                        }
                        else {
                            if( this.style.color != '' )
                                jQuery(this).attr('data-acwp-txtcolor', this.style.color);
                            this.style.color = 'white';
                        }
                    });
                }
            }
            else {
                // imgs
                jQuery('body *').each(function () {
                    if( this.style.backgroundImage != '' )
                        this.style.backgroundImage = '';
                });
                jQuery('body [data-acwp-bgimage]').each(function () {
                    let bg = jQuery(this).attr('data-acwp-bgimage');
                    if( bg != '' )
                        this.style.backgroundImage = bg;
                });
                // bgs
                jQuery('body *').each(function () {
                    if( this.style.backgroundColor != '' )
                        this.style.backgroundColor = '';
                });
                jQuery('body [data-acwp-bgcolor]').each(function () {
                    let bg = jQuery(this).attr('data-acwp-bgcolor');
                    if( bg != '' )
                        this.style.backgroundColor = bg;
                });

                // txt
                jQuery('body *').each(function () {
                    if( this.tagName == 'a' || this.tagName == 'button' || this.tagName == 'label' ) {
                        let clr = jQuery(this).attr('data-acwp-lnkcolor');
                        if( clr != '' )
                            this.style.color = clr;
                        if( this.style.color != '' )
                            this.style.color = '';
                    }
                    else {
                        let clr = jQuery(this).attr('data-acwp-txtcolor');

                        if( this.style.color != '' )
                            this.style.color = '';
                        if( clr && clr != '' )
                            this.style.color = clr;
                    }
                });
                document.body.classList.remove('acwp-contrast');
            }
        }
    }
    render() {

        let classValue = "acwp-toggler acwp-toggler-" + this.props.name;
        let inputId = "acwp-toggler-" + this.props.name;
        let label = this.props.label;
        let icon;

        if(this.props.name === 'contrast')
            icon = 'nights_stay';
        else if(this.props.name === 'monochrome')
            icon = 'format_color_reset';
        else if(this.props.name === 'underline')
            icon = 'format_underlined';
        else if(this.props.name === 'marklinks')
            icon = 'insert_link';
        else if(this.props.name === 'marktitles')
            icon = 'title';
        else if(this.props.name === 'decfont')
            icon = 'text_fields';
        else if(this.props.name === 'incfont')
            icon = 'format_size';
        else if(this.props.name === 'readable')
            icon = 'font_download';
        else if(this.props.name === 'animations')
            icon = 'visibility_off';
        else if(this.props.name === 'keyboard')
            icon = 'keyboard';

        let togglerName = this.props.name;



        return (
            <div className={classValue}>
                <label htmlFor={inputId} onClick={((e) => this.labelClick(e, togglerName))} tabIndex="0">
                    <i className="material-icons">{icon}</i><span>{label}</span>
                    <div className="acwp-switcher">
                        <input type="checkbox" hidden="hidden" id={inputId} />
                        <div className="acwp-switch"></div>
                    </div>
                </label>

            </div>
        );

    }
}