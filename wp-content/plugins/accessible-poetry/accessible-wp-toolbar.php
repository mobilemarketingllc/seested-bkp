<?php
/**
 * Plugin Name: AccessibleWP Toolbar
 * Plugin URI: https://wordpress.org/plugins/accessible-poetry/
 * Description: Make it easy for your users with an accessibility toolbar for people with disabilities at all levels. The Accessible WP Toolbar uses modern technologies and has minimal affect on your site performance.
 * Author: Amit Moreno
 * Author URI: https://www.amitmoreno.com/
 * Version: 4.0.4
 * Text Domain: acwp
 * License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

if( ! defined( 'ABSPATH' ) )
    return;

// Define our directory
define('WPRS_DIR', plugin_dir_url( __FILE__ ));

// Require what we need ...
require_once 'inc/assets.php';
require_once 'inc/main-component.php';
require_once 'inc/route.php';
require_once 'inc/panel.php';


/**
 * Load toolbar textdomain
 */
function acwp_load_textdomain() {
    load_plugin_textdomain( 'acwp', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );
}
add_action( 'init', 'acwp_load_textdomain' );
